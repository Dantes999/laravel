<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discussion extends Model
{
    use SoftDeletes;
    protected $connection = 'mysql3';
    protected $table = 'discussion';
    public $timestamps = true;
    protected $fillable = ['title', 'category_id', 'user_id', 'slug', 'color'];
    protected $dates = ['deleted_at', 'last_reply_at'];

    public function user()
    {
        return $this->setConnection('mysql')->belongsTo(config('chatter.user.namespace'));
    }

    public function category()
    {
        return $this->belongsTo(Models::className(Category::class), 'category_id');
    }

    public function posts()
    {
        return $this->hasMany(Models::className(Post::class), 'discussion_id');
    }

    public function post()
    {
        return $this->hasMany(Models::className(Post::class), 'discussion_id')->orderBy('created_at', 'ASC');
    }

    public function postsCount()
    {
        return $this->posts()
        ->selectRaw('discussion_id, count(*)-1 as total')
        ->groupBy('discussion_id');
    }

    public function users()
    {
        return $this->belongsToMany(config('chatter.user.namespace'), 'discussion', 'discussion_id', 'user_id');
    }
}
