<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Discussion2 extends Model
{
    use SoftDeletes;
    protected $connection = 'mysql';
    public $timestamps = true;
    protected $fillable = ['title', 'category_id', 'user_id', 'slug', 'color'];
    protected $dates = ['deleted_at', 'last_reply_at'];

    public static function user()
    {
        return $this->belongsTo(config('chatter.user.namespace'));
    }

    public static function users()
    {
        return $this->belongsToMany(config('chatter.user.namespace'), 'discussion', 'discussion_id', 'user_id');
    }

}
