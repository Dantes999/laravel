<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Symfony\Component\Console\Input\Input;
use Illuminate\Support\Facades\Auth;

class RegistrationController extends Controller
{
    public function create()
    {
        return view('pages.register');
    }
    public function store()
    {
        $this->validate(request(), [
            'login' => 'bail|alpha_num|required|max:16|min:8|unique:mysql.account',
            'email' => 'required|email|confirmed',
            'password' => 'required|max:30|min:6|confirmed',
            'social_id' => 'required|max:7'
        ]);

        $user = User::firstOrCreate(request(['login', 'email', 'password','social_id']));

        var_dump($user->wasRecentlyCreated);
        Auth::attempt(['login' => request()->login, 'password' => \App\User::mysql_password_hash(request()->password)], request()->has('remember'));
        //auth()->login($user);
        return redirect()->to('/user');

    }
}
