<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function create()
    {
        return view('pages.landing');
    }
    public function admin()
    {
        if(Auth::user()->isAdmin === 'true')
            return view('pages.admin');
        else
            return view('pages.home');
    }
    public function language($locale){
        Session::put('locale', $locale);
        return redirect()->back();
    }
    public function createRanking()
    {
        $ranking = DB::connection('mysql2')
                        ->table('player')
                        ->select('name','playtime','level','gold')
                        ->orderBy('level', 'desc')
                        ->get();
        return view('pages.ranking',['ranking' => $ranking]);
    }
    public function createDownload()
    {
        return view('pages.download');
    }
}
