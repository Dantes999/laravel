<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\User;

class AdminController extends Controller
{
    public function create()
    {
        if(Auth::user()->isAdmin === 'true')
            return view('pages.admin');
        else
            return view('pages.home');
    }
    public function store(Request $request)
    {
        if(Auth::user()->isAdmin === 'true'){
            $request->validate([
                'id'    =>  ['required'],
                'new_password' => ['required'],
                'new_confirm_password' => ['same:new_password'],
            ]);

            if(User::where('login',$request->id)->first()){
                User::where('login',$request->id)->first()->update(['password'=> $request->new_password]);
                return back()->withErrors([
                    'message' => 'Passwort erfolgreich geändert'
                ]);
            }

            return back()->withErrors([
                'message' => 'User existiert nicht'
            ]);


        }
    }
}
