<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Closure;
use App\User;
use Illuminate\Foundation\Auth\ThrottlesLogins;

class LoginController extends Controller
{

    public function create()
    {
        return view('pages.login');
    }

    public function store()
    {
        $request = request();

        #Check LocalHost for ReCaptcha
        $whitelist = array(
            '127.0.0.1',
            '::1'
        );
        if(!in_array($_SERVER['REMOTE_ADDR'], $whitelist)){
            $url = 'https://www.google.com/recaptcha/api/siteverify';
            $remoteip = $_SERVER['REMOTE_ADDR'];
            $data = [
                    'secret' => config('services.recaptcha.secret'),
                    'response' => $request->get('recaptcha'),
                    'remoteip' => $remoteip
                ];
            $options = [
                    'http' => [
                    'header' => "Content-type: application/x-www-form-urlencoded\r\n",
                    'method' => 'POST',
                    'content' => http_build_query($data)
                    ]
                ];
            $context = stream_context_create($options);
            $result = file_get_contents($url, false, $context);
            $resultJson = json_decode($result);

            if ($resultJson->success != true)
                return back()->withErrors(['captcha' => 'ReCaptcha Error 1']);

            if ($resultJson->score >= 0.3) {
                $user = \App\User::where([
                    'login' => request()->login,
                    'password' => \App\User::mysql_password_hash(request()->password),

                ])->first();
                /*The first argument passed to the method is the number of records you wish to receive per "chunk".
                The Closure passed as the second argument will be called for each chunk that is retrieved from the database.
                A database query will be executed to retrieve each chunk of records passed to the Closure. */
                if($user){
                    //Auth::attempt(['login' => request()->login, 'password' => \App\User::mysql_password_hash(request()->password)], request()->has('remember'));
                    //auth()->login($user);
                    if( Auth::attempt(['login' => request()->login, 'password' => \App\User::mysql_password_hash(request()->password)], request()->has('remember'))){//if(auth()->user()){ // fetching Session
                        return redirect()->to('/user');
                    }
                    else{
                        return back()->withErrors([
                            'message' => 'The ID or password is incorrect, please try again'
                        ]);
                    }

                }
                else{
                    return back()->withErrors([
                        'message' => 'The ID or password is incorrect, please try again'
                    ]);
                }
            }
            else{
                return back()->withErrors(['captcha' => 'ReCaptcha Error 2']);
            }
        }
        else{
            $user = User::where([
                'login' => request()->login,
                'password' => User::mysql_password_hash(request()->password),

            ])->first();
            if($user){
                auth()->login($user);
                if(auth()->user()){ // fetching Session
                    return redirect()->to('/home');
                }
                else{
                    return back()->withErrors([
                        'message' => 'The ID or password is incorrect, please try again'
                    ]);
                }

            }
            else{
                return back()->withErrors([
                    'message' => 'The ID or password is incorrect, please try again'
                ]);
            }
        }
    }

    public function destroy()
    {
        auth()->logout();
        return redirect()->to('/');
    }

}
