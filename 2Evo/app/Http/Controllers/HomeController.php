<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\App;

class HomeController extends Controller
{
    public function create()
    {
        return view('pages.home');
    }
}
