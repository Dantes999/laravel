<?php

namespace App\Http\Controllers;

use DateTime;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;


class UserController extends Controller
{
    public function create()
    {
        $charID = Auth::user()->id;
        if(!$charID)
        {
            return back()->withErrors(['message' => 'Bitte wähle einen Charakter!']);
        }

        $loggedInData = DB::connection('mysql2')
                        ->table('player')
                        ->select('id','name')
                        ->where('account_id', $charID)
                        ->limit(5)
                        ->get();
        return view('pages.user',['loggedInData' => $loggedInData]);
    }

    public function unstuckPOST()
    {
        $charID = request();

        if(!$charID->charachterid)
        {
            return back()->withErrors(['message' => 'Bitte wähle einen Charakter!']);
        }

        $id = DB::connection('mysql2')
                ->table('player')
                ->select('account_id','last_play')
                ->where('id', $charID->charachterid)
                ->limit(1)
                ->get();
        $date1 = new DateTime($id[0]->last_play);
        $tmp = date('Y-m-d H:i:s');
        $date2 = new DateTime($tmp);
        if(date_diff($date1,$date2)->i < 5){
                    return back()->withErrors([
                        'message' => 'Der Charakter ist erst seit '.(string)date_diff($date1,$date2)->i.' Minuten ausgeloggt. Du musst noch '.(string)(5-date_diff($date1,$date2)->i).' Minuten warten',
                    ]);
        }
        if(Auth::user()->id === $id[0]->account_id){
            #hole Empire
            $empire = DB::connection('mysql2')
                        ->table('player_index')
                        ->select('empire')
                        ->where('id', $id[0]->account_id)
                        ->limit(1)
                        ->get();
            $empire = ($empire[0]->empire);
            switch($empire)
            {
                case 3:
                {
                    $x =966300 + rand(-300, 300);
                    $y =288300 + rand(-300, 300);
                    $map_index = 41;
                }break;
                case 2:
                {
                    $x =45700 + rand(-300, 300);
                    $y =166500 + rand(-300, 300);
                    $map_index = 21;
                }break;
                case 1:
                {
                    $x =457100 + rand(-300, 300);
                    $y =946900 + rand(-300, 300);
                    $map_index = 1;
                }break;
                default:
                    $x = 457100 + rand(-300, 300);
                    $y = 946900 + rand(-300, 300);
                    $map_index = 1;
                    break;
            }
            $empire = DB::connection('mysql2')
                        ->table('player')
                        ->where('id', $charID->charachterid)
                        ->update(['x' => $x,'y' => $y,'map_index' => $map_index]);
            if( $empire){
                return back()->withErrors([
                    'message' => 'Der Charakter wurde erfolgreich entbuggt.'
                ]);
            }
            else{
                return back()->withErrors([
                    'message' => 'Der Charakter wurde nicht entbuggt.'
                ]);
            }
        }

    }
}
