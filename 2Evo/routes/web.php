<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Redirect;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



#Diese Routen(web) sind frei zugänglich
Route::group(['middleware' => 'web'], function () {

    Route::get('/', 'Controller@create');

    Route::get('/login', 'Controller@create')->name('login');

    Route::post('/login', 'LoginController@store')->middleware("throttle:5,1");

    Route::get('/register', 'Controller@create')->name('register');
    Route::post('/register', 'RegistrationController@store')->middleware("throttle:5,1");

    Route::get('/download', 'Controller@createDownload');

    Route::get('locale/{locale}', 'Controller@language');

    Route::get('notification', 'Controller@notification');

    #Diese Routen(auth) sind nur als eingeloggter User zugänglich
    Route::group(['middleware' => 'auth'], function () {
        Route::get('/admin', 'AdminController@create');
        Route::get('/logout', 'LoginController@destroy');
        Route::get('/home', 'HomeController@create');
        Route::get('/ranking', 'Controller@createRanking');

        Route::get('/', 'UserController@create');

        Route::get('/user', 'UserController@create');
        Route::post('/user', 'UserController@unstuckPOST');
        Route::get('/forum', 'ChatterController@index');

        Route::post('change-password', 'ChangePasswordController@store')->name('change.password');
        Route::post('change-password-admin', 'AdminController@store')->name('change.password.admin');
        // Single category view.
        Route::get('/forum/category/{slug}','ChatterController@index')->name('category.show');
        //Discussion
        Route::get('/forum/discussion/','ChatterDiscussionController@show')->name('show');
        Route::get('/forum/discussion/create','ChatterDiscussionController@create')->name('create');
        Route::post('/forum/discussion/','ChatterDiscussionController@store')->name('store');
        Route::get('/forum/discussion/{category}/{slug}','ChatterDiscussionController@show')->name('showInCategory');
        Route::post('/forum/discussion/{category}/{slug}/email','ChatterDiscussionController@toggleEmailNotification')->name('email');
        Route::get('/forum/discussion/edit','ChatterDiscussionController@edit')->name('edit');
        Route::match(['PUT', 'PATCH'],'/forum/discussion/','ChatterDiscussionController@update')->name('update');
        Route::delete('/forum/discussion/','ChatterDiscussionController@destroy')->name('destroy');
        //Post
        Route::post('/forum/posts/','ChatterPostController@store');
        Route::get('/forum/posts/edit/{post}','ChatterPostController@edit');
        Route::match(['PUT', 'PATCH'],'/forum/posts/{post}','ChatterPostController@update');
        Route::delete('/forum/posts/{post}','ChatterPostController@destroy');
    });
});

#leitet jede andere Route auf /login um. Z.b. /admin /hack /... alle
Route::any('/{any}', 'LoginController@create')->where('any', '.*');
