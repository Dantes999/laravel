<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateChatterPostTable extends Migration
{
    public function up()
    {
        Schema::connection('mysql3')->create('post', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->integer('discussion_id')->unsigned()->references('id')->on('discussion')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->integer('user_id')->unsigned()->references('id')->on('account.account')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->text('body');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::connection('mysql3')->drop('post');
    }
}
