<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateForeignKeys extends Migration
{
    public function up()
    {
        Schema::connection('mysql3')->table('discussion', function (Blueprint $table) {
            $table->foreign('category_id')->references('id')->on('categories')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('account')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');
        });
        Schema::connection('mysql3')->table('post', function (Blueprint $table) {
            $table->foreign('discussion_id')->references('id')->on('discussion')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('account')
                        ->onDelete('cascade')
                        ->onUpdate('cascade');
        });
    }

    public function down()
    {
        Schema::connection('mysql3')->table('discussion', function (Blueprint $table) {
            $table->dropForeign('discussion_category_id_foreign');
            $table->dropForeign('discussion_user_id_foreign');
        });
        Schema::connection('mysql3')->table('post', function (Blueprint $table) {
            $table->dropForeign('post_discussion_id_foreign');
            $table->dropForeign('post_user_id_foreign');
        });
    }
}
