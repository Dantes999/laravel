<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
class CreateChatterDiscussionTable extends Migration
{
    public function up()
    {
        Schema::connection('mysql3')->create('discussion', function (Blueprint $table) {
            $table->engine = 'MyISAM';
            $table->increments('id');
            $table->integer('category_id')->unsigned()->default('1')->references('id')->on('categories')
            ->onDelete('cascade')
            ->onUpdate('cascade');
            $table->string('title')->default('Kein Titel');
            $table->integer('user_id')->unsigned()->index()->references('id')->on('account.account')
            ->onDelete('cascade')
            ->onUpdate('cascade');
			$table->integer('discussion_id')->default('0')->unsigned()->index()->references('id')
			->on('discussion')
			->onDelete('cascade');
            $table->boolean('sticky')->default(false);
            $table->integer('views')->unsigned()->default('0');
            $table->boolean('answered')->default(0);
            $table->timestamps();
			//$table->primary(['user_id', 'discussion_id']);
        });
    }

    public function down()
    {
        Schema::connection('mysql3')->drop('discussion');
    }
}
