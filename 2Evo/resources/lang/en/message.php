<?php
return [
    'welcome'   => 'Welcome to 2Evolution',
    'login'     => 'Login',
    'logout'    => 'Logout',
    'register'  => 'Register',
    'username'  => 'Logged in with',
    'id'        => 'Username',
    'pass'      => 'Password',
    'pass2'     => 'Password confirmation',
    'submit'    => 'Submit',
    'genPass'   => 'generate password',
    'regiDis'   => 'register is disabled',
    'forumDis'  => 'forum is disabled',
    'ranking'   => 'Ranking',
    'download'  => 'Download',
    'playtime'  => 'Playtime',
    'delCode'   => 'Delete code',
    'email2'    => 'Email confirmation',
    'forum'     => 'Board',
    'repeat'    => 'repeat',
    'delcode'   => '7 numbers',
    'specChar'  => '1 special character',
    'idSpec'    => '8-16 characters',

    'words' => [
        'cancel'  => 'Cancel',
        'delete'  => 'Delete',
        'edit'    => 'Edit',
        'yes'     => 'Yes',
        'no'      => 'No',
        'minutes' => '1 minute| :count minutes',
    ],

    'discussion' => [
        'new'          => 'New discussion',
        'all'          => 'All discussions',
        'create'       => 'Create discussion',
        'posted_by'    => 'Posted by',
        'head_details' => 'Posted in Category',

    ],
    'response' => [
        'confirm'     => 'Are you sure you want to delete this response?',
        'yes_confirm' => 'Yes Delete It',
        'no_confirm'  => 'No Thanks',
        'submit'      => 'Submit response',
        'update'      => 'Update Response',
    ],

    'editor' => [
        'title'               => 'Title of discussion',
        'select'              => 'Select a Category',
        'tinymce_placeholder' => 'Type Your discussion Here...',
        'select_color_text'   => 'Select a Color for this discussion (optional)',
    ],

    'email' => [
        'notify' => 'Notify me when someone replies',
    ],

    'auth' => 'Please <a href="/:home/login">login</a>
                or <a href="/:home/register">register</a>
                to leave a response.',
];
