<?php

return [
    'success' => [
        'title'  => 'Gut gemacht!',
        'reason' => [
            'submitted_to_post'       => 'Antwort erfolgreich zum Thema gestellt.',
            'updated_post'            => 'Das Thema wurde erfolgreich aktualisiert.',
            'destroy_post'            => 'Antwort und Thema erfolgreich gelöscht.',
            'destroy_from_discussion' => 'Die Antwort aus diesem Thema wurde erfolgreich gelöscht.',
            'created_discussion'      => 'Erfolgreich eine neues Thema erstellt.',
        ],
    ],
    'info' => [
        'title' => 'Kopf hoch!',
    ],
    'warning' => [
        'title' => 'Wuh Oh!',
    ],
    'danger'  => [
        'title'  => 'Oh verdammt!',
        'reason' => [
            'errors'            => 'Bitte behebe die folgenden Fehler:',
            'prevent_spam'      => 'Um Spam zu vermeiden, warte bitte mindestens :Minuten zwischen dem posten von Inhalten.',
            'trouble'           => 'Entschuldigung, es scheint ein Problem beim Senden Ihrer Antwort aufgetreten zu sein.',
            'update_post'       => 'Nah ah ah... Antwort konnte nicht aktualisiert werden. Stell sicher, dass du nichts falsches machst.',
            'destroy_post'      => 'Nah ah ah... Antwort konnte nicht gelöscht werden. Stell sicher, dass du nichts falsches machst.',
            'create_discussion' => 'Whoops :( Es scheint ein Problem bei der Erstellung deines Themas zu geben.',
        	'title_required'    => 'Bitte schreibe einen Titel.',
        	'title_min'		    => 'Der Titel muss mindestens :min Zeichen haben.',
        	'title_max'		    => 'Der Titel darf nicht mehr als :max Zeichen haben.',
        	'content_required'  => 'Bitte schreibe ein wenig Inhalt.',
        	'content_min'  		=> 'Der Inhalt muss mindestens :min Zeichen haben.',
        	'category_required' => 'Bitte wähle eine Kategorie.',

        ],
    ],
];
