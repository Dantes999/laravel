<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'reset' => 'Dein Passwort wurde zurückgesetzt!',
    'sent' => 'Du hast eine Email mit dem neuen Passwort bekommen!',
    'throttled' => 'Bitte warte noch ein wenig.',
    'token' => 'Dieser Passwort-Reset Token ist ungültig.',
    'user' => "Wir könnten keinen User mit dieser Email finden.",

];
