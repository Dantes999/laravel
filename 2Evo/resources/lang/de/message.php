<?php
return [
    'welcome'   => 'Willkommen auf 2Evolution',
    'login'     => 'Einloggen',
    'logout'    => 'Ausloggen',
    'register'  => 'Registrieren',
    'username'  => 'Eingeloggt mit ',
    'id'        => 'Benutzername',
    'pass'      => 'Passwort',
    'pass2'     => 'Passwort wiederholen',
    'submit'    => 'Absenden',
    'genPass'   => 'Passwort generieren',
    'regiDis'   => 'Die Registrierung ist deaktiviert',
    'forumDis'  => 'Das Forum ist deaktiviert',
    'ranking'   => 'Rangliste',
    'download'  => 'Download',
    'playtime'  => 'Spielzeit',
    'delCode'   => 'Löschcode',
    'email2'    => 'Email wiederholen',
    'forum'     => 'Forum',
    'repeat'    => 'wiederholen',
    'delcode'   => '7 Zahlen',
    'specChar'  => '1 Sonderzeichen',
    'idSpec'    => '8-16 Buchstaben',

    'words' => [
        'cancel'  => 'Abbrechen',
        'delete'  => 'Löschen',
        'edit'    => 'Bearbeiten',
        'yes'     => 'Ja',
        'no'      => 'Nein',
        'minutes' => '1 Minute| :count Minuten',
    ],

    'discussion' => [
        'new'          => 'Neues Thema',
        'all'          => 'Alle Themen',
        'create'       => 'Thema erstellen',
        'posted_by'    => 'Geschrieben von',
        'head_details' => 'Kategorie:',

    ],
    'response' => [
        'confirm'     => 'Möchtest du diese Antwort wirklich löschen?',
        'yes_confirm' => 'Ja lösche es!',
        'no_confirm'  => 'Nein, danke.',
        'submit'      => 'Antwort senden',
        'update'      => 'Antwort aktualisieren',
    ],

    'editor' => [
        'title'               => 'Titel des Themas',
        'select'              => 'Wähle eine Kategorie',
        'tinymce_placeholder' => 'Hier kommt dein Text rein...',
        'select_color_text'   => 'Wähle eine Farbe für dein Thema (optional)',
    ],

    'email' => [
        'notify' => 'Benachrichtige mich, wenn jemand antwortet',
    ],

    'auth' => 'Please <a href="/:home/login">login</a>
                or <a href="/:home/register">register</a>
                to leave a response.',

];
