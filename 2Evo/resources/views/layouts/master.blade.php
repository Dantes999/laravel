<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <?php echo "<link rel='stylesheet' type='text/css' href='css/style4.css?".mt_rand()."'>"; ?>
        <!--<link href="{{ url('/css/style4.css?".mt_rand()."') }}" rel="stylesheet">-->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

        <meta name="csrf-token" content="{{ csrf_token() }}" />

        <title>2Evo</title>

        @yield('head')

    </head>
    <body>
        <div id="wrapper">
            <div id="header">
                @include('includes.navbar')
            </div>
            <div id="left-sidebar"></div>
            <div id="inner-content">
                @yield('content')
            </div>
            <div id="right-sidebar"></div>
        </div>
        <script src="{{ asset('js/app.js') }}"></script>
        @yield('js')
    </body>
</html>
