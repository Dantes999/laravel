
<div class="topnav" id="myTopnav">
    <a href="/" id="logo"><img src="<?php echo asset("img/logo_3.png")?>"></a>
    @if( Auth::user() )
        <a class="nav-item" href="/user">{{ __('message.username') }} <span style="color: red"> {{ auth()->user()->login }} </span></a>
        <a class="nav-item" href="/forum">{{ __('message.forum') }}</a>
        <a class="nav-item" href="/ranking">{{ __('message.ranking') }}</a>
        <a class="nav-item" href="/download">{{ __('message.download') }}</a>
        <a class="nav-item" href="/logout">{{ __('message.logout') }}</a>
    @else
        <a class="nav-item" style="cursor: pointer" id="loginModalBTN"> {{ __('message.login') }}</a>
        <a class="nav-item" href="/download">{{ __('message.download') }}</a>
        <!--<a class="nav-item" href="/register">{{ __('message.register') }}</a>-->
    @endif
    <a class="nav-item" href="{{ url('locale/en') }}" ><i class="fa fa-language"></i> <img style="width:35px" src="<?php echo asset("img/en.png")?>"/></a>
    <a class="nav-item" href="{{ url('locale/de') }}" ><i class="fa fa-language"></i> <img style="width:32px" src="<?php echo asset("img/de.png")?>"/></a>
    <a href="javascript:void(0);" class="icon" onclick="myFunction()">&#9776;</a>
    <script>
        function myFunction() {
          var x = document.getElementById("myTopnav");
          if (x.className === "topnav") {
            x.className += " responsive";
          } else {
            x.className = "topnav";
          }
        }
    </script>
</div>
