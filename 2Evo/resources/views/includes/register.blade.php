<head>
    @if (env('RECAPTCHA_SITE_KEY') and $_SERVER['REMOTE_ADDR'] != '127.0.0.1')
        <script src="https://www.google.com/recaptcha/api.js?render={{ config('services.recaptcha.sitekey') }}"></script>
        <script>
                grecaptcha.ready(function() {
                    grecaptcha.execute('{{ config('services.recaptcha.sitekey') }}', {action: 'contact'}).then(function(token) {
                        if (token) {
                        document.getElementById('recaptcha').value = token;
                        }
                    });
                });
        </script>
    @endif
    <script>
        function generatePassword(){
            var result           = '';
            var characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@_-+~#§$%&/()=?\}][{<>,.:;*';
            var charactersLength = characters.length;
            for ( var i = 0; i < 8; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            document.getElementById('password_1').value = result;
            document.getElementById('password_confirmation').value = result;
            document.getElementById('newPass').innerText = result;
        }
    </script>
</head>
<div id="register-form">
    <form method="POST" action="/register" style="max-width: 300px; min-width: 150; padding-top: 25%">
        @csrf
        @if(env('DISABLE_REGISTER') === false)
            <div class="form-group" >
                <input placeholder={{ __('message.id') }}  type="text" class="form-control" id="login" name="login">
                <a class="inputText">{{ __('message.idSpec') }}</a>
            </div>

            <div class="form-group">
                <input placeholder='Email' type="email" class="form-control" id="email" name="email">
            </div>

            <div class="form-group">
                <input placeholder='{{ __('message.email2') }}'  type="email" class="form-control" id="email_confirmation"
                    name="email_confirmation">
                    <a class="inputText">{{ __('message.email2') }}</a>
            </div>

            <div class="form-group">
                <a class="inputText" id="genPassBtn" onclick="generatePassword()" style="color: #DBB667">{{ __('message.genPass') }}</a>
                <a id="newPass"class="inputText"> </a>
            </div>

            <div class="form-group">
                <input placeholder={{ __('message.pass') }}  type="password" class="form-control" id="password_1" name="password">
                <a class="inputText">{{ __('message.specChar') }}</a>
            </div>

            <div class="form-group">
                <input placeholder='{{ __('message.pass2') }}'  type="password" class="form-control" id="password_confirmation"
                    name="password_confirmation">
                    <a class="inputText">{{ __('message.pass2') }}</a>
            </div>

            <div class="form-group">
                <input placeholder={{ __('message.delCode') }} type="text" class="form-control" id="social_id" name="social_id">
                <a class="inputText">{{ __('message.delcode') }}</a>
            </div>
            <div class="form-group">
                <label for="remember" id="rememberfield"><input type="checkbox" name="remember" id="remember" value="1">Remember Me</label>
            </div>
            @if (env('RECAPTCHA_SITE_KEY') and $_SERVER['REMOTE_ADDR'] != '127.0.0.1')
                <input type="hidden" name="recaptcha" id="recaptcha">
            @endif


            <button id="registerBtn" style="cursor:pointer" type="submit" class="btn" ><img  src="<?php echo asset("img/register-norm.png")?>"onmouseover="this.src='img/register-hover.png'" onmouseout="this.src='img/register-norm.png'"></button>


            @if($errors->any())
                <h4 style="color: red">{{$errors->first()}}</h4>
            @endif
        @else
            <div class="alert alert-warning d-flex justify-content-center">{{ __('message.regiDis') }}</div>
        @endif
    </form>
</div>
