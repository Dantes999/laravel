
<head>
    @if (env('RECAPTCHA_SITE_KEY'))
        <script src="https://www.google.com/recaptcha/api.js?render={{ config('services.recaptcha.sitekey') }}"></script>
        <script>
                grecaptcha.ready(function() {
                    grecaptcha.execute('{{ config('services.recaptcha.sitekey') }}', {action: 'contact'}).then(function(token) {
                        if (token) {
                            document.getElementById('recaptcha').value = token;
                        }
                    });
                });
        </script>
    @endif

</head>

<body>
<div class="modal" id="loginModal" style="display: none;" tabindex="-1" role="dialog" aria-labelledby="loginModal" aria-hidden="true" data-backdrop="false">
    <div class="modal-content">
        <div class="modal-body">
            <span class="close">&times;</span>
            <form method="POST" action="{{ route('login') }}" id="login-form-text">
                @csrf
                <div class="form-group2">
                    <input class="input-field" placeholder={{ __('message.id') }} type="username" class="form-control @error('username') is-invalid @enderror" id="login" name="login" required  autofocus>
                </div>

                <div class="form-group2">
                    <input class="input-field" placeholder={{ __('message.pass') }} type="password" class="form-control" id="password" name="password">
                </div>
                <div class="form-group2">
                    <label for="remember" id="rememberfield"><input type="checkbox" name="remember" id="remember" value="1">Remember Me</label>
                </div>
                <button id="loginModalBtn2" type="submit"><img  src="<?php echo asset("img/Login-norm.png")?>" onmouseover="this.src='img/Login-hover.png'" onmouseout="this.src='img/Login-norm.png'"></button>

                @if($errors->any())
                    <h4 style="color: red">{{$errors->first()}}</h4>
                @endif

                @if (env('RECAPTCHA_SITE_KEY') and $_SERVER['REMOTE_ADDR'] != '127.0.0.1')
                    <input type="hidden" name="recaptcha" id="recaptcha">
                @endif

            </form>
        </div>
    </div>
</div>
<script>
    var modal = document.getElementById("loginModal");
    var btn = document.getElementById("loginModalBTN");
    var span = document.getElementsByClassName("close")[0];

    btn.onclick = function() {
        modal.style.display = "block";
        $( "#login" ).focus();
    }

    span.onclick = function() {
        modal.style.display = "none";
    }

    window.onclick = function(event) {
        if (event.target == modal) {
            modal.style.display = "none";
        }
    }
</script>
</body>
