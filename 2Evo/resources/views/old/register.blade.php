@extends('layouts.master')

<head>
    @if (env('RECAPTCHA_SITE_KEY') and $_SERVER['REMOTE_ADDR'] != '127.0.0.1')
        <script src="https://www.google.com/recaptcha/api.js?render={{ config('services.recaptcha.sitekey') }}"></script>
        <script>
                grecaptcha.ready(function() {
                    grecaptcha.execute('{{ config('services.recaptcha.sitekey') }}', {action: 'contact'}).then(function(token) {
                        if (token) {
                        document.getElementById('recaptcha').value = token;
                        }
                    });
                });
        </script>
    @endif
    <script>
        function generatePassword(){
            var result           = '';
            var characters       = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!@_-+~#§$%&/()=?\}][{<>,.:;*';
            var charactersLength = characters.length;
            for ( var i = 0; i < 8; i++ ) {
                result += characters.charAt(Math.floor(Math.random() * charactersLength));
            }
            document.getElementById('password').value = result;
            document.getElementById('password_confirmation').value = result;
            document.getElementById('newPass').innerText = result;
        }
    </script>
</head>
<script>
    window.onload = function() {
        document.getElementById('download-btn').style.display = 'none';
        //document.getElementById('login-container').style.display = 'none';
    };
</script>
@section('register-form')
<form method="POST" action="/register" style="max-width: 300px; min-width: 150;">
    @csrf
    @if(env('DISABLE_REGISTER') === false)
        <div class="form-group">
            <input placeholder={{ __('message.id') }}  type="text" class="form-control" id="login" name="login">
        </div>

        <div class="form-group">
            <input placeholder='Email' type="email" class="form-control" id="email" name="email">
        </div>

        <div class="form-group">
            <input placeholder={{ __('message.email2') }}  type="email" class="form-control" id="email_confirmation"
                name="email_confirmation">
        </div>

        <div class="form-group">
            <a class="btn btn-secondary" onclick="generatePassword()">{{ __('message.genPass') }}</a>
            <a id="newPass" style="background-color: white"> </a>
        </div>

        <div class="form-group">
            <input placeholder={{ __('message.pass') }}  type="password" class="form-control" id="password" name="password">
        </div>

        <div class="form-group">
            <input placeholder={{ __('message.pass2') }}  type="password" class="form-control" id="password_confirmation"
                name="password_confirmation">
        </div>

        <div class="form-group">
            <input placeholder={{ __('message.delCode') }} type="text" class="form-control" id="social_id" name="social_id">
        </div>

        @if (env('RECAPTCHA_SITE_KEY') and $_SERVER['REMOTE_ADDR'] != '127.0.0.1')
            <input type="hidden" name="recaptcha" id="recaptcha">
        @endif

        <div class="form-group">
            <button style="cursor:pointer" type="submit" class="btn "><img  src="<?php echo asset("img/register-norm.png")?>"onmouseover="this.src='img/register-hover.png'" onmouseout="this.src='img/register-norm.png'"></button>
        </div>

        @if($errors->any())
            <h4 style="color: red">{{$errors->first()}}</h4>
        @endif
    @else
        <div class="alert alert-warning d-flex justify-content-center">{{ __('message.regiDis') }}</div>
    @endif
</form>
@endsection
