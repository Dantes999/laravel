@extends('layouts.master')
@section('title', 'Dashboard')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Passwort Ändern</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('change.password.admin') }}">
                        @csrf

                         @foreach ($errors->all() as $error)
                            <p style="color:red;">{{ $error }}</p>
                         @endforeach

                        <div class="form-group row">
                            <label for="id" >User Name</label>

                            <div class="col-md-6">
                                <input id="id" type="id" class="form-control" name="id" autocomplete="id">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" >New Password</label>

                            <div class="col-md-6">
                                <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" >New Confirm Password</label>

                            <div class="col-md-6">
                                <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@stop

