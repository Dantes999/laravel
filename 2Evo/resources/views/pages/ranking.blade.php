@extends('layouts.master')

@section('content')
    <div class="table-responsive">
        <table class="table" style="backdrop-filter: contrast(20%);">
            <thead class="thead-light">
                <tr>
                <th scope="col">Name</th>
                <th scope="col">{{ __('message.playtime') }}</th>
                <th scope="col">Level</th>
                <th scope="col">Gold</th>
                </tr>
            </thead>

            <tbody>
                @foreach($ranking as $key => $data)
                    @if( strpos($data->name, '[') !== false)
                    @else
                        <tr>
                            <th>{{$data->name}}</th>
                            <th>{{$data->playtime}}</th>
                            <th>{{$data->level}}</th>
                            <th>{{$data->gold}}</th>
                        </tr>
                    @endif
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
