@extends('layouts.master')

@section('content')
@include('includes.login')
    <div class="table-responsive">
        <table class="table" style="backdrop-filter: contrast(20%);">
            <thead class="thead-light">
                <tr>
                <th scope="col">Hoster</th>
                <th scope="col">Link</th>
                </tr>
            </thead>

            <tbody>
                <tr>
                    <th><img src="<?php echo asset("/logo/mega.png")?>" style="width:30px;"></th>
                    <th><a href="https://www.mega.nz" style="display:block;" target="_blank">Download</a></th>
                </tr>

            </tbody>
        </table>
    </div>
@endsection
