@extends('layouts.master')

<head>
    @if (env('RECAPTCHA_SITE_KEY'))
        <script src="https://www.google.com/recaptcha/api.js?render={{ config('services.recaptcha.sitekey') }}"></script>
        <script>
                grecaptcha.ready(function() {
                    grecaptcha.execute('{{ config('services.recaptcha.sitekey') }}', {action: 'contact'}).then(function(token) {
                        if (token) {
                            document.getElementById('recaptcha').value = token;
                        }
                    });
                });
        </script>
    @endif

</head>
<script>
    window.onload = function() {
        document.getElementById('download-btn').style.display = 'none';
        document.getElementById('register-container').style.display = 'none';
    };
</script>
@section('login-form')
<form method="POST" action="/login" style="max-width: 300px; min-width: 150;">
    @csrf
    <div class="form-group">
        <input placeholder={{ __('message.id') }} type="username" class="form-control @error('username') is-invalid @enderror" id="login" name="login" required  autofocus>
    </div>

    <div class="form-group">
        <input placeholder={{ __('message.pass') }} type="password" class="form-control" id="password" name="password">
    </div>

    <div class="form-group">
        <button style="cursor:pointer" type="submit" class="btn"><img  src="<?php echo asset("login-nrom_2.png")?>"></button>
    </div>

    @if($errors->any())
        <h4 style="color: red">{{$errors->first()}}</h4>
    @endif

    @if (env('RECAPTCHA_SITE_KEY') and $_SERVER['REMOTE_ADDR'] != '127.0.0.1')
        <input type="hidden" name="recaptcha" id="recaptcha">
    @endif
</form>
@endsection
