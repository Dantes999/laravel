@extends('layouts.master')

@section('content')
<div id="userpage">
    <h1>User-Verwaltung</h1>

    <div class="accordion" id="accordionUser">
        <div class="bordered" style="backdrop-filter: contrast(20%);">
        <div class="card-header" id="headingOne2">
            <h5 class="mb-0">
            <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#collapseOne2"
                aria-expanded="true" aria-controls="collapseOne2" style="color:black;">
                Charaktere
            </button>
            </h5>
        </div>
        <div id="collapseOne2" class="collapse show" aria-labelledby="headingOne2"
            data-parent="#accordionUser">
            <div class="card-body">
                <div class="postui post-con">
                    <div class="con-wrap">
                    <p>Hier kannst du deinen Charakter zurücksetzen falls er feststeckt. Wähle einfach und bestätige deine Eingabe.</p>
                    <p>Dein Charakter sollte vorher mindestens 5 Minuten ausgeloggt gewesen sein.</p>
                        @if($errors->any())
                            <h4 style="color: red">{{$errors->first()}}</h4>
                        @endif
                        <form class="unstuck" action="{{ action('UserController@unstuckPOST') }}" method="POST">
                            @csrf
                            <select name="charachterid"  id="charID">
                            <option value="0">Bitte Auswählen</option>
                            @foreach( $loggedInData as $player)
                                <option value="{{ $player->id }}">{{ $player->name }}</option>
                            @endforeach
                            </select>
                            <input type="submit" name="submit" value="Bestätigen" class="btn" style="display:inline;">

                        </form>
                        <div class="sep"></div>

                    </div>
                </div>
                <div class="postui post-end"></div>
                </div>
            </div>
        </div>
        </div>

        <div class="bordered" style="backdrop-filter: contrast(20%);">
            <div class="card-header" id="headingThree3">
                <h5 class="mb-0">
                    <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                    data-target="#collapseThree3" aria-expanded="false" aria-controls="collapseThree3" style="color:black;">
                    Passwort-Ändern
                    </button>
                </h5>
            </div>
            <div id="collapseThree3" class="collapse" aria-labelledby="headingThree3" data-parent="#accordionUser">
                <div class="card-body">
                    <form method="POST" action="{{ route('change.password') }}">
                        @csrf

                        @foreach ($errors->all() as $error)
                            <p class="text-danger">{{ $error }}</p>
                        @endforeach

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">Current Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="current_password" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Password</label>

                            <div class="col-md-6">
                                <input id="new_password" type="password" class="form-control" name="new_password" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="password" class="col-md-4 col-form-label text-md-right">New Confirm Password</label>

                            <div class="col-md-6">
                                <input id="new_confirm_password" type="password" class="form-control" name="new_confirm_password" autocomplete="current-password">
                            </div>
                        </div>

                        <div class="form-group row mb-0">
                            <div class="col-md-8 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    Update Password
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="bordered" style="backdrop-filter: contrast(20%);">
            <div class="card-header" id="headingTwo2">
                <h5 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                    data-target="#collapseTwo2" aria-expanded="false" aria-controls="collapseTwo2" style="color:black;">
                    Itemshop
                </button>
                </h5>
            </div>
            <div id="collapseTwo2" class="collapse" aria-labelledby="headingTwo2"
                data-parent="#accordionUser">
                <div class="card-body">
                Kommt noch
                </div>
            </div>
            </div>
            <div class="bordered" style="backdrop-filter: contrast(20%);">
            <div class="card-header" id="headingThree2">
                <h5 class="mb-0">
                <button class="btn btn-link collapsed" type="button" data-toggle="collapse"
                    data-target="#collapseThree2" aria-expanded="false" aria-controls="collapseThree2" style="color:black;">
                    Spenden
                </button>
                </h5>
            </div>
            <div id="collapseThree2" class="collapse" aria-labelledby="headingThree2"
                data-parent="#accordionUser">
                <div class="card-body">
                    Kommt noch
                </div>
            </div>
            </div>
    </div>
</div>
@endsection
