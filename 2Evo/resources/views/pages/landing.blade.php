@extends('layouts.master')
<script>
    window.onload = function() {
        //document.getElementById('login-container').style.display = 'none';
        document.getElementById('register-container').style.display = 'none';
    };
</script>
@section('content')

@include('includes.login')

<div id="download-btn">
    <a href="/download"><img  src="img/download-normal_2.png"onmouseover="this.src='img/download-hover.png'" onmouseout="this.src='img/download-normal_2.png'"></a>
</div>


<div class="row" id="landing-container">
    <div class="col" id="info-left" style="color: DBB667">
        <h4 class="info-titel">Systeme</h4>
        <div class="info-text">
            <p>- Offline-Shop</p>
            <p>- OffShop-Suche</p>
            <p>- Transmutation</p>
            <p>- MiniMe</p>
            <p>- Schulterband</p>
            <p>- Event</p>
            <p>- WarpMap</p>
            <p>- Ingame-Wiki</p>
        </div>
    </div>
    <div class="col" id="info-right" style="color: DBB667">
        <h4 class="info-titel">Zu uns</h4>
        <div class="info-text">
            <p>Wir (Syntax und Volvox) haben schon sehr viele P-Server gespielt.</p>
            <p>Uns sind immer mehr Unstimmigkeiten aufgefallen.</p>
            <p>Dann haben wir uns dazu entschlossen, ein eigenes Projekt zu starten</p>
            <p>Dieses Projekt ist 2Evolution ❤️</p>
            <p>Wir betreiben den Server nicht, damit wir viel Geld damit bekommen !</p>
            <p>Einen stabilen, Bug-freien und Botter-freien Server wollen wir erschaffen.</p>
            <p>Wir freuen uns auf euch !</p>
        </div>
    </div>
</div>

<div id="register-text">
    <img src="img/register-now.png">
    <p>Werde Teil der Alpha !</p>
    <p>Wir starten am 17.07.2020</p>
</div>

<div id="register-container" class="container">
    @yield('register-form')
</div>

@include('includes.register')

@endsection

